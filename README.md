# Experiments with Physics Engines

I have found a part-time fascination with physics engines. It is a mix between what I am studying, programming, with my other hobby, digital art.

I will use this repo as a sandbox-journal of sorts, documenting my various implementations and sketch-tests.

## How do I see the examples? ##

All implementations are organized in the source folder under their respective names.

### Position Based Dynamics Implementation

- **pbd-js** : Javascript implementation 

## The Physics Engine 

### Position Based Dynamics
The paper by [Muller, et al. (2006)](http://matthias-mueller-fischer.ch/publications/posBasedDyn.pdf) 
illustrates a vertex position iterative method where positions are calculated directly. This is different
from the force/acceleration approach most physics simulator employ.

What drew me to the algorithm was its ease of implementation and its ability to be parallelized 
on a multi-core GPU platform.

The researchers have followed up the work with several more [papers](http://matthias-mueller-fischer.ch/). 
There is also an open-source C++ implementation provided by Jan Bender found [here](https://github.com/janbender/PositionBasedDynamics).