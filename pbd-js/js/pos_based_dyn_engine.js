(function(undefined) {
"use strict";

var
_ARR_UTIL = {
    'removeAll': function (ar) {
        /**
        Removes all objects in the array, nulling all references
        to queued objects.
        
        :param: ar - An array object. 
        */
        ar.forEach(function s(v,i) { ar[i] = null; });
        ar.splice(0, ar.length);
    }
};

// REDO: change to static calc functions for more room.
/**
2D Vector
*/
function Vector(x,y) {
    this.x = x || 0;
    this.y = y || 0;
}

Vector.prototype = {
    x : 0,
    y : 0,
    add : function(vect) {
        return new Vector(this.x + vect.x, this.y + vect.y);
    },
    incr : function(vect) {
        this.x += vect.x; 
        this.y += vect.y;
    },
    sub : function(vect) {
        return new Vector(this.x - vect.x, this.y - vect.y);
    },
    divs : function(k) {
        return new Vector(this.x / k, this.y / k);
    },
    muls : function(k) {
        return new Vector(this.x * k, this.y * k);
    },
    muls_incr : function (s) {
        this.x *= s;
        this.y *= s;
    },
    magnitude : function() {
        var 
        x = this.x,
        y = this.y;
        return Math.pow((x*x + y*y),0.5);
    },
    magnitudeSqrd : function() {
        var 
        x = this.x,
        y = this.y;
        return x*x + y*y;
    },
    normal : function() {
        var 
        v = new Vector(),
        mag = this.magnitude();
        v.x = (this.x / mag) || 0;
        v.y = (this.y / mag) || 0;
        return v;
    },
    copy : function() {
        return new Vector(
            this.x,
            this.y);
    },
    equals: function(other) {
        this.x = other.x;
        this.y = other.y;
    },
    reflect : function(normal) {
        /**
        Return the reflected incident on a given normal.
        
        Derived using the following equation:
        
            I' = I - 2(N.I) * N
        
        where:
            I' is the resulting reflection vector (this),
            I is the incident vector,
            N is the acting normal at point of collision
        
        The (.) indicates dot product, (*) indicates scalar multiplication.
        
        @param (incident) 2D incident vector
        @param (normal) 2D normal vector
        @return Return the resulting 2D reflection vector
        */
        return this.sub(
            normal.muls(
                2 * (this.x * normal.x + this.y * normal.y)));
    },
    dot : function (v) {
        /**
        Returns the dot product of this vector with the parameter vector.
        
        @param (v) 2D vector
        @return Returns the dot product of 2D vectors
        */
        return this.x * v.x + this.y * v.y;
    },
    tostring : function () {
        return ("( " + this.x + ", " + this.y +" )");
    }
    
}; // Vector

function Bodies () {
    /**
    This a creator / collection object for physics bodies. This object defines 
    the following functionality:
    1) Body creation
    2) Various body iterator views
    */
    var
    _extend = function (target, ext) {
        for (var n in ext) {
            // BUG: will break if target is array
            target[n] = ext[n];
        }
        function t() {};
        t.prototype = target;
        return new t();
    },
    /**
        The following lists the sub-objects that build the supported objects.
        "Inheritance" is index defined, meaning the higher indexed  
        names will take higher precedence in the prototype tree.
        */
    _OBJ_CLS_ = Object.freeze({
        'circle': ['_basic_','_movement-1v_','circle'],
        'line': ['_basic_','line'],
        'chain': ['_basic_','has-elements','chain'],
        'chain-link': ['_basic_','_movement-1v_','chain-link'],
        'cohesion': ['_basic_','_movement-1v_','has-elements','cohesion'],
        'cohesion-elem': ['_basic_','_movement-1v_','circle','cohesion-elem'],
    }),
    // TODO: make this a constructor maker to not have it go through the hierarchy
    _composeBuilder = function (defaults) {
    
        return function (shape, obj) {
            var 
            rtn = {}, _cls_list = _OBJ_CLS_[shape];
            _cls_list.forEach(function t(name) {
                rtn = _extend(rtn, defaults[name](obj))
            });
            return rtn;
        };
    },
    _builder = _composeBuilder({ 
        '_basic_': function (opts) {
            return {
                'guid': 'NEED TO DO THIS',
                'id': opts.id || 0,
                'shape': opts.shape || undefined,
                'type': opts.type || 'static',
                'draw': opts.draw || function () {},
                '__classList': [].concat(_OBJ_CLS_[opts.shape] || [undefined]),
                '__inBounds': function (bounds) { return true; },
                '__destroy': function () {
                    var obj = this;
                    // destroy children
                    if (obj.__classList.indexOf('has-elements') > -1) {
                        obj.elems.forEach(function t(e,i) {
                            e.__destroy();
                            obj.elems[i] = null;
                        });
                    }
                    // clean out vectors
                    if (obj.__classList.indexOf('_movement-1v_') > -1 || obj.__classList.indexOf('line') > -1) {
                        ['origin','velocity','projected','dprojected'].forEach(function s(n) {
                            obj[n].forEach(function t(v,i) {
                                obj[n][i] = null;
                            });
                        });
                    } 
                    // clean up
                    obj = null;
                } 
            };
        },
        '_movement-1v_': function (opts) {
            return {
                '__inBounds': function (bounds) { 
                    var
                    x = this.origin[0].x,
                    y = this.origin[0].y;
                    
                    return (
                        // left and top
                        x >= bounds[0] && y >= bounds[1]
                        // right and bottom
                        && x <= bounds[2] && y <= bounds[3]);                            
                },
                'mass': opts.mass || 0,
                'origin': [].concat((opts.origin) ? 
                    new Vector(opts.origin[0],opts.origin[1]) : 
                    new Vector()),
                'velocity': [].concat((opts.velocity) ? 
                    new Vector(opts.velocity[0],opts.velocity[1]) : 
                    new Vector()),
                'projected': [].concat((opts.projected) ? 
                    new Vector(opts.projected[0],opts.projected[1]) : 
                    new Vector()),
                'dprojected': [].concat((opts.dprojected) ? 
                    new Vector(opts.dprojected[0],opts.dprojected[1]) : 
                    new Vector()) };
        },
        'circle': function (opts) {
            return {
                'radius': opts.radius || 10 };
        },
        'line': function () {
            return {
                'origin': [].concat(new Vector()),
                'velocity': [].concat(new Vector()),
                'projected': [].concat(new Vector()),
                'dprojected': [].concat(new Vector()) };
        },
        'chain': function () {
            return {
                // Chains are collections, and are not included
                // in the comparison pairings when iterating. 
                '_doNotPair': true };
        },
        'has-elements': function (opts) {
            return {
                'elems': [] };
        },
        'chain-link': function (opts) {
            return {
                'radius': opts.radius || 10,
                'linkLength': opts.linkLength || 10,
                'stiffness': opts.stiffness || .95,
            };
        },
        'cohesion': function (opts) {
            return {
                'minDistance': opts.minDistance || 50,
            };
        },
        'cohesion-elem': function (opts) {
            return {
                'stiffness': opts.stiffness || .8,
                'damping': opts.damping || 1.0
            };
        }
    }),
    SELECT_MODE = Object.freeze({
        'All': 0,
        'Skip_No_Pairs': 1, // REDO: does anything?
        'Skip_All_Children': 2
    }),
    _flattenCollection = function (list, rtn, mode) {
        rtn = rtn || [];
        list.forEach(function s(obj) {
            if (undefined !== obj.elems) {
                if (mode == SELECT_MODE.Skip_All_Children) {
                    rtn.push(obj);
                    return;
                }
                _flattenCollection(obj.elems, rtn);
            }
            // REDO: this is too complicated...
            if (mode == SELECT_MODE.All || !obj._doNotPair) { 
                rtn.push(obj);
            }        
        });
    },
    _iterators = Object.freeze({ 
        'only_children': function (list) {
            /**
            Iterates through the elements of a collection of groups. 
            
            When the group's element list is exhausted, the iterator 
            switches to the next group in the list, beginning at index 0.
            */
            var
            chainIndex = 0, linkIndex = 0;
            return {
                next: function () {
                    
                    if (list.length == 0) return { done:true };
                    // did we reach the end of the chain (group)?
                    // if so, move to the beginning of the next chain.
                    if (linkIndex >= list[chainIndex].elems.length) {
                        chainIndex++;
                        linkIndex = 0;
                    }
                    // BUG: will fail on successive empty groups.
                    return (chainIndex < list.length) ?
                        { value: list[chainIndex].elems[linkIndex++], done: false } :
                        { done: true };
                },
                reset: function () {
                    chainIndex = 0; linkIndex = 0;
                }
            };
        },
        'chained_sibling_pairs': function (list) {
            var
            chainIndex = 0, linkIndex = 0;
            return {
                next: function () {
                    
                    if (list.length == 0) return { done:true };
                    
                    // did we reach the end of the chain?
                    // if so, move to the beginning of the next chain.
                    if ((linkIndex + 1) > (list[chainIndex].elems.length - 1)) {
                        chainIndex++;
                        linkIndex = 0;
                    }
                    return (chainIndex < list.length) ?
                        { 
                            // returns a chain's adjacent pairs of links
                            // BUG: This will fail if successive chains have one link.
                            value: [
                                list[chainIndex].elems[linkIndex], 
                                list[chainIndex].elems[++linkIndex]], 
                            done: false,
                        } :
                        { done: true };
                },
                reset: function () {
                    chainIndex = 0; linkIndex = 0;
                }
            };
        },
        'parent_elements': function (list) {
            /**
            Creates an iterator with pairing the parent object with
            its element collection.
            
            The parent object is assumed to have a list object names
            'elems'.
            */
            var
            groupIndex = 0, elemIndex = 0;
            return {
                next: function () {
                    if (list.length == 0) return { done:true };
                    // move to the beginning of the next group.
                    if (elemIndex >= list[groupIndex].elems.length) {
                        groupIndex++; elemIndex = 0;
                    }
                    return (groupIndex < list.length) ?
                        { value: [
                              // parent
                              list[groupIndex],
                              // parent's element
                              list[groupIndex].elems[elemIndex++]], 
                          done: false 
                        } :
                        { done: true };
                },
                reset: function () {
                    groupIndex = 0; elemIndex = 0;
                }
            };
        },
        'all': function (list) {
            /**
            Creates an iterator that lists all bodies. 
            */
            var
            bodies = [], elemIndex = 0;
            return {
                next: function () {
                    return (elemIndex < bodies.length && 0 != list.length) ?
                        { value: bodies[elemIndex++],  done: false } :
                        { done: true };
                },
                reset: function (opts) {
                    elemIndex = 0;
                    bodies.splice(0, bodies.length);
                    if (opts.parentsOnly) {
                        _flattenCollection(list, bodies, SELECT_MODE.Skip_All_Children);
                    } else {
                        _flattenCollection(list, bodies, SELECT_MODE.All);
                    }
                }
            };
        },
        'all_global': function (lists) {
            var bodies = [], start = 0, i = 1;
            return {
                next: function () {
                    
                    if (i >= bodies.length) {
                        start++;
                        i = start + 1;
                    }
                    
                    return (start < (bodies.length - 1)) ?
                        { value: [bodies[start], bodies[i++]], done: false } :
                        { done: true };
                },
                reset: function () {
                    start = 0, i = 1;
                    bodies.splice(0, bodies.length);
                    lists.forEach(function (lst) { 
                        _flattenCollection(lst, bodies, 
                            SELECT_MODE.Skip_No_Pairs);
                    });
                }
            };
        }
    }),
    _composeView = function (iter, coll) {
        return iter(coll);
    },
    _collectionAdapter = function (collection) {
        
        // TODO: this needs to be globalized
        var getClass = {}.toString;

        return function (names) {
            if ('[object String]' == getClass.call(names)) {
                return collection[names];
            }
            if ('[object Array]' == getClass.call(names)) {
                return names.map(function s(name) {
                    // returns an array of arrays
                    return collection[name];
                });
            }
        };
    },
    _buildViews = function (iterators, collections, config) {
        
        var 
        views = {},
        aggregator = _collectionAdapter(collections);
        
        Object.keys(config).forEach(function s(name) {
            views[name] = config[name].map(function t(a) {
                return _composeView(iterators[a[0]], aggregator(a[1]));
            });
        });
        return views;
    };
    
    function ctor () {
        var
        _collections = {
            'circle': [], 'line': [],
            'chain': [], 'cohesion': []
        },
        // REDO: Too many iterators makes it confusing to add on new objects!
        _views = _buildViews(
            _iterators, _collections, 
            {
                'individual': [
                    ['all','circle'],
                    ['all','line'],
                    ['all','cohesion'],
                    ['only_children','chain'] ],
                'all': [
                    ['all','circle'],
                    ['all','line'],
                    ['all','cohesion'],
                    ['all','chain'] ],
                // for intra-body constraint iteration
                'internal': [
                    ['chained_sibling_pairs','chain'],
                    ['parent_elements','cohesion'] ],
                // For inter-body constraint iteration. Skips collecion items.
                'body-pair': [
                    ['all_global',['chain','circle','cohesion']] ]
            });
        
        return Object.freeze({
            'add': function (obj) {
                _collections[obj.shape].push(obj);
            },
            'clear': function () {
                this.forEach('all', function s(body) {
                    body.__destroy();
                });
                Object.keys(_collections).forEach(function s(name) {
                    _ARR_UTIL.removeAll(_collections[name]);
                });
            },
            'create': function (obj) {
                return _builder(obj.shape, obj);
            },
            'forEach': function (name, func, opts) {
                opts = opts || {};
                (_views[name] || []).forEach(function s(view) {
                    // Reset (initialize) the view or nothing will happen!
                    view.reset(opts);
                    for (var res = view.next(opts); !res.done; res = view.next(opts)) {
                        func(res.value);
                    }
                });
            },
            'remove': function (body) {
                var 
                _coll = _collections[body.shape],
                _item = _coll.splice(_coll.indexOf(body), 1);
                _item[0].__destroy();
                _item[0] = null;
            }
        });
    }
    return ctor();
} // Bodies

function PhysicsEngine(bodies, collisionDetector, clusterConstraintMgr, opts) {
    /**
    Projects the position of bodies. 

    Reference: 
    Müller, M., et al. Position Based Dynamics. 3rd Workshop in Virtual 
    Reality Interactions and Physical Simulation "VRIPHYS" (2006)
    
    ** Algorithm Summary **
    (1) forall vertices i
    (2)     initialize xi = x0i; vi = v0i; wi = 1/mi
    (3) endfor
    (4) loop
    (5)     forall vertices i do vi += dt*wi*Fext(xi)
    (6)     dampVelocities(v1,...,vN)
    (7)     forall vertices i do pi = xi + dt*vi
    (8)     forall vertices i do generateCollisionConstraints(xi -> pi)
    (9)     loop solverIterations times
    (10)        projectConstraints(C1,...,CM+Mcoll; p1,...,pN)
    (11)    endloop
    (12)    forall vertices i
    (13)        vi = (pi - xi) / dt
    (14)        xi = pi
    (15)    endfor
    (16)    velocityUpdate(v1,...,vN)
    (17) endloop
    
    :param: bodies - Collection object
    :param: collisionDetector - Generates position projections due to 
    inter-body collisions.
    :param: clusterConstraintManager - Generates position projections
    due to intra-body constraints.
    :param: opts - Physics engine configuration settings.
    */
    var
    _physConfig = {
		INCIDENT_REDUCT_FACTOR : 0.17,
		NUM_COLLISION_CALC_ITER : 5,
		COLLISION_ADJ_FACTOR : 0.65,
    };
    
    function _ctor(bodies, opts) {
        
        var
        _world_bounds = [
            // upper-left  (x,y)
            -150,-100,        
            // lower-right (x,y)
             650, 450 ],
        _static_force_vectors = [],
        _dynamic_constraints = [],
        _phys = {
            projectStaticForces: function(elapsedTime) {
                bodies.forEach('individual', function s(body) {
                    if (undefined === body.velocity
                        || 0 == body.mass 
                        || 'static' == body.type 
                        || 'ghost' == body.type) 
                    { 
                        return; 
                    }
                    body.velocity.forEach(function t(vel) {
                        var vel_update = elapsedTime * (1.0 / body.mass);
                        _static_force_vectors.forEach(function u(force) {
                            vel.incr(
                                force.muls(vel_update));
                        });
                    });
                });
                return this;
            },
            dampVelocities: function() {
                bodies.forEach('individual', function s(body) {
                    if (undefined === body.velocity || 'ghost' == body.type) 
                        return;
                    // no damping for springs means oscillation (0,1)
                    // BUG: stiffness destroys static force induced velocities 
                    body.velocity.forEach(function t(v) {
                        v.muls_incr(body.stiffness || 1.0);
                    });
                });
                return this;
            },
            projectPosition: function(elapsedTime) {
                bodies.forEach('individual', function s(body) {
                    if (undefined === body.velocity) return; 
                    body.velocity.forEach(function t(vel, i) {
                        var 
                        // p = (v * dt) + o
                        p = vel.muls(elapsedTime).add(body.origin[i]);
                        body.projected[i].equals(p);
                        p = null;
                    });
                });
                return this;
            },
            generateInternalCollisionConstraints: function () {
                bodies.forEach('internal', function s(a) {
                    var constraint = clusterConstraintMgr.evaluate(a[0], a[1]);
                    if (constraint) {
                        _dynamic_constraints.push(constraint);
                    }
                    constraint = null;
                });
                return this;
            },
            generateCollisionsConstraints: function () {
                bodies.forEach('body-pair', function s(a) {
                    var projfunc = collisionDetector.evaluate(a[0], a[1]);
                    if (projfunc) {
                        _dynamic_constraints.push(projfunc);
                    }
                });
                return this;
            },
            projectConstraints: function() {
                // iterate to get to stable position
                for (var b = 0, ben = _physConfig.NUM_COLLISION_CALC_ITER; b < ben; b++) {
                    // apply dynamic constraints
                    _dynamic_constraints.forEach(function s(func) {
                        func({ 
                            numIter: _physConfig.NUM_COLLISION_CALC_ITER,
                            collisionFactor: _physConfig.COLLISION_ADJ_FACTOR });
                    });
                }
                return this;
            },
            update: function(elapsedTime) {
                bodies.forEach('individual', function s(body) {
                    // skip bodies that don't move
                    if (undefined === body.velocity || 'static' == body.type) 
                        return;
                    body.velocity.forEach(function t(vel, i) {
                        var 
                        bo = body.origin[i],
                        // p' = p + dp
                        p = body.projected[i],
                        // v = (p' - o) / dt
                        v = p.sub(body.origin[i]).divs(elapsedTime);
                        // v' <- v
                        vel.equals(v);
                        // o' <- p'
                        bo.equals(p);
                        // clean up
                        bo = p = v = null;
                    });
                });
                return this;
            },
            velocityUpdate: function() {
                // TODO
                return this;
            },
            reset: function() {
                _ARR_UTIL.removeAll(_dynamic_constraints);
                // remove out of bounds objects
                bodies.forEach('all', 
                    function s(body) {
                        if (!body.__inBounds(_world_bounds)) {
                            bodies.remove(body);
                        }
                    }, { parentsOnly: true });
                return this;
            },
            clear: function () {
                _ARR_UTIL.removeAll(_dynamic_constraints);
                _ARR_UTIL.removeAll(_static_force_vectors);
                return this;
            }
        };
    
        return Object.freeze({
            addForce: function (force) {
                _static_force_vectors.push(force);
            },
            step: function (elapsedTime) {
                elapsedTime = (elapsedTime) ? elapsedTime : .05;
                _phys
                    // apply static forces
                    .projectStaticForces(elapsedTime)
                    // reduce velocities to prevent overshoots
                    .dampVelocities()
                    // project position using velocity
                    .projectPosition(elapsedTime)
                    // correct intra-body constraints
                    .generateInternalCollisionConstraints()
                    // generate inter-body collision constraints
                    .generateCollisionsConstraints()
                    // project constraints
                    .projectConstraints()
                    // update projection with delta projection, and
                    // update position and velocity for next round
                    .update(elapsedTime)
                    // scale down velocities due to restitution 
                    // coefficients, friction, damping, etc...
                    .velocityUpdate()
                    // clear engine for next iteration
                    .reset();
            },
            clear: _phys.clear
        });
    }
    return _ctor(bodies, opts);
} // PhysicsEngine

function CollisionConstraintEmitter(types) {
    /**
    Framework to emit constraint projects when collisions are detected.
    */
    
    var
    _supportedShapes = {};
	
    types.forEach(function s(v,i) {
        _supportedShapes[v] = Math.pow(2,i);
    });
    
    function _ctor() {
        var
        _supported_collisions = {};
        
        return Object.freeze({
            register: function (obj) {
                /**
                Registers collision evaluator objects for detection.
                
                @param obj The object that comprises the evaluator. The parameter
                must have the following components:
                
                {
                    collisionTypes: ['circle','circle'],
                    collisionTest: function (bodyA, bodyB) {
                        return true | false
                    },
                    collisionEmitter: function (bodyA, bodyB, collPtEst) {
                        return function(opts)
                    },
                    collisionPointEst: function () {
                        return coll // TODO...
                    }
                }
                */
                var 
                _composeCollisionEvalFunc = function (respFunc, testFunc, collPtEst) {
                    testFunc = (testFunc) ? testFunc : function s() { return true; };
                    return function t(bodyA, bodyB) {
                        if (testFunc(bodyA, bodyB)) {
                            return respFunc(bodyA, bodyB, collPtEst);
                        }
                        return false;
                    };
                };
                obj = (undefined === obj.length) ? [obj] : obj;
                obj.forEach(function s(o) {
                    var 
                    // Create a unique key to reference the collision evaluator.
                    // Body pairs are OR'd together to calculate the key and
                    // pull the constraint evaluator.
                    key = _supportedShapes[o.type[0]] | _supportedShapes[o.type[1]];
                    _supported_collisions[key] = _composeCollisionEvalFunc(
                        o.emitter,
                        o.test,
                        o.collisionPointEst);
                });
              
            },
            evaluate: function (bodyA, bodyB) {
                
                if (undefined == _supportedShapes[bodyA.shape] || 
                    undefined == _supportedShapes[bodyB.shape]) 
                {
                    // one or both of the shape collisions is not defined
                    return false;
                }
                
                var 
                f = _supported_collisions[
                    _supportedShapes[bodyA.shape] | 
                    _supportedShapes[bodyB.shape]];
            
                return (f) ? f(bodyA, bodyB) : false;
            }
        });
    }
    return _ctor();
} // CollisionConstraintEmitter

function ClusterConstraintEmitter(types) {

    var
    _supportedTypes = {};
    
    // assigns a unique code to supported types
    types.forEach(function s(v,i) {
        _supportedTypes[v] = Math.pow(2,i);
    });
    
    function ctor() {
        
        var
        _clusteredConstraints = {};
        
        return Object.freeze({
            register: function (obj) {
                obj = (undefined === obj.length) ? [obj] : obj;
                obj.forEach(function s(o) {
                    var 
                    key = _supportedTypes[o.type[0]] | _supportedTypes[o.type[1]];
                    _clusteredConstraints[key] = function (bodyA, bodyB) {
                        if (o.test(bodyA, bodyB)) {
                            return o.emitter(bodyA, bodyB);
                        }
                        return false;
                    }
                });
            },
            evaluate: function (bodyA, bodyB) {
                var f = _clusteredConstraints[
                    _supportedTypes[bodyA.shape] |
                    _supportedTypes[bodyB.shape]];
                return (f) ? f(bodyA, bodyB) : false;
            }
        });
    }
    return ctor();
} // ClusterConstraintEmitter

function World(bodies, physics) {

    function _ctor(bodies, physics) {
        
        var
        _timer = false,
        _opts = null,
        _privf = {
            execute_step: function s() {}
        },
        _world = Object.freeze({
            add: function (opts) {
                bodies.add(opts);
            },
            clear: function () {
                bodies.clear();
                physics.clear();
            },
            createNewBody: function (opts) {
                return bodies.create(opts);
            },
            draw: function () {
                bodies.forEach('all', function s(b) {
                    b.draw(b);
                });
            },
            execute: function (func) {
                // TODO: use a composition function to grab this...
                _privf.execute_step = func;
            },
            setGravity: function (x, y) {
                physics.addForce(new Vector(x,y));
            },
            start: function () {
                if (_timer) return;
                _timer = window.setInterval(
                    _privf.execute_step, 15);
            },
            step: function (elapsedTime) {
                physics.step(elapsedTime);
            },
            stop: function () {
                if (!_timer) return;
                window.clearInterval(_timer);
                _timer = false;
            }
        });
        return _world;
    }
    return _ctor(bodies, physics);
} // World

/* Common tests */

/* Constraint Declarations */

var
clusterConstraints = [
{
    type: ['cohesion','cohesion-elem'],
    test: function (bodyA, bodyB) {
        var
        rel_dist = bodyA.origin[0]
            .sub(bodyB.origin[0])
            .magnitude();
            //.magnitudeSqrd(),
        return rel_dist > bodyA.minDistance;
    },
    emitter: function (bodyA, bodyB) {
        return function s(opts) {
            
            var
            // draw a line between the two origins (L)
            // measure the distance between the origins
            link = bodyA.origin[0]
                .sub(bodyB.origin[0]),
            dist = link.magnitude();
            
            if (dist <= bodyA.minDistance) {
                link = null;
                return;
            }
            
            var
            travel = dist - bodyA.minDistance,
            norm = link.normal(),
            // (<=).1 for the links seems to calm down the model
            corrB = norm.muls(bodyB.damping * travel * .1);
            
            if ('dynamic' == bodyB.type) {
                bodyB.projected[0].incr(corrB);
            }
            link = norm = corrB = null;
        }
    }
},
{
    type: ['chain-link','chain-link'],
    test: function (bodyA, bodyB) {
        var
        rel_dist = bodyA.origin[0]
            .sub(bodyB.origin[0])
            .magnitude(),
            //.magnitudeSqrd(),
        diff = rel_dist - bodyA.linkLength;
        return ((diff > 1.0) || (diff < -1.0));
    },
    emitter: function (bodyA, bodyB) {
        return function s(opts) {
            var
            // draw a line between the two origins (L)
            // measure the distance between the origins
            link = bodyA.origin[0]
                .sub(bodyB.origin[0]),
            // the sign of diff will attract or repulse the two bodies.
            diff = link.magnitude() - bodyA.linkLength;
            
            if (0 == diff) return;
            
            var
            norm = link.normal(),
            // (<=).1 for the links seems to calm down the model
            corrA = norm.muls(diff * .1 * -1.0),
            corrB = norm.muls(diff * .1);
            
            if ('dynamic' == bodyA.type) {
                var pA = bodyA.projected[0];
                pA.x += corrA.x;
                pA.y += corrA.y;
            }
            if ('dynamic' == bodyB.type) {
                var pB = bodyB.projected[0];
                pB.x += corrB.x;
                pB.y += corrB.y;
            }
        }
    }
}],
collisionConstraints = [
{
    type: ['circle','circle'],
    test : function (bodyA, bodyB) {
        var 
        rA = bodyA.radius,
        rB = bodyB.radius,
        dist = rA + rB;
        return bodyA.projected[0]
            .sub(bodyB.projected[0])
            .magnitudeSqrd() <= (dist * dist);
    },
    collisionPointEst : function (bodyA, bodyB, numIter) {
        /**
        Estimates the collision point between two circle bodies.
        
        Collision point is calculated using [burk?] iterative
        estimation.
        
        Finding collision point (circle on circle ONLY):
           1) find the incident vector (p - x) 
           2) reduce incident by a FACTOR:
              
                  |incident| *= FACTOR
              
           3) loop back to step 2 until max number of iterations 
              is met, or, distance between circles is greater than
              the sum of their respective radii.
           4) return collision point
   
        @param (bodyA) Incident body. 
        @param (bodyB) "Static" body.  (Does not move.)
        @return Returns a 2D vector indicating the collision point.
        */
        var
        y = bodyB.origin[0]
            .sub(bodyA.origin[0]),
        i = bodyA.projected[0]
            .sub(bodyA.origin[0]),
        alpha = Math.acos(i.dot(y) / (y.magnitude() * i.magnitude())), 
        z = y.magnitude() * Math.sin(alpha),
        beta = Math.asin(z / (bodyA.radius + bodyB.radius)),
        d = (z / Math.tan(beta)) || 0,
        x = (z / Math.tan(alpha)) || 0,
        Dell = x - d,
        // new position 
        pp = i.normal()
            .muls(Dell)
            .add(bodyA.origin[0]),
        c = bodyB.origin[0]
            .sub(pp)
            .normal()
            .muls(bodyA.radius / (bodyA.radius + bodyB.radius))
            .add(bodyA.origin[0]);
        return c;
    },
    emitter: function (bodyA, bodyB, collPointEst) {
        var 
        _calcMassRatio = function (m1, m2) {
            return (m1 && m2) ? m1 / (m1 + m2) : 0;
        };
        // this function gets iteratively called, therefore
        // needs a test for collision
        return function s(opts) {
            var 
            // point of collision
            c = collPointEst(bodyA, bodyB, opts.numIter),
            // incident vector 
            i = bodyA.projected[0]
                .sub(bodyA.origin[0])
                .sub(bodyB.projected[0].sub(bodyB.origin[0]))
                .muls(1),
            // collision normals (n)
            na = c.sub(bodyA.origin[0])
                .normal(),
            // BUG: IF BODYA LIES ON BODYB HALF, THEN UNDEFINED
            nb = c.sub(bodyB.origin[0])
                .normal(),
            // scale deflections from to 2-body conservation of momentum
            massRatio = _calcMassRatio(bodyB.mass, bodyA.mass),
            // deflect body A
            // BUG: SHOULDN'T THIS BE THE FULL DISTANCE?
            na1 = -.275 * (i.dot(na)),
            xA = na.muls(na1 * (massRatio || 1)),
            // deflect body B
            // BUG: SHOULDN'T THIS BE THE FULL DISTANCE?
            nb1 = .275 * (i.dot(nb)), 
            xB = nb.muls(nb1 *(1 - massRatio));
            
            // remove penetration
            var
            dell1 = bodyA.projected[0].sub(bodyB.projected[0]).magnitude(),
            // the larger diff1 is, the more the bodies are pushed apart
            diff1 = dell1 - (bodyA.radius + bodyB.radius),
            chgxA = xA.normal()
                .muls(
                    Math.abs((diff1/2)) * opts.collisionFactor),
            chgxB = chgxA.muls(-1);
            
            // Update projected
            if ('dynamic' === bodyA.type) {
                bodyA.projected[0].x += (xA.x + chgxA.x) ;
                bodyA.projected[0].y += (xA.y + chgxA.y);
            }
            
            if ('dynamic' === bodyB.type) {
                bodyB.projected[0].x += (xB.x + chgxB.x);
                bodyB.projected[0].y += (xB.y + chgxB.y);
            }
        };
    } 
},
{
    type: ['circle','chain-link'],
    test : function (bodyA, bodyB) {
        var 
        rA = bodyA.radius,
        rB = bodyB.radius,
        dist = rA + rB;
        return bodyA.projected[0]
            .sub(bodyB.projected[0])
            .magnitudeSqrd() <= (dist * dist);
    },
    collisionPointEst : function (bodyA, bodyB, numIter) {
        /**
        Estimates the collision point between two circle bodies.
        
        Collision point is calculated using [burk?] iterative
        estimation.
        
        Finding collision point (circle on circle ONLY):
           1) find the incident vector (p - x) 
           2) reduce incident by a FACTOR:
              
                  |incident| *= FACTOR
              
           3) loop back to step 2 until max number of iterations 
              is met, or, distance between circles is greater than
              the sum of their respective radii.
           4) return collision point
   
        @param (bodyA) Incident body. 
        @param (bodyB) "Static" body.  (Does not move.)
        @return Returns a 2D vector indicating the collision point.
        */
        var
        y = bodyB.origin[0]
            .sub(bodyA.origin[0]),
        i = bodyA.projected[0]
            .sub(bodyA.origin[0]),
        alpha = Math.acos(i.dot(y) / (y.magnitude() * i.magnitude())), 
        z = y.magnitude() * Math.sin(alpha),
        beta = Math.asin(z / (bodyA.radius + bodyB.radius)),
        d = (z / Math.tan(beta)) || 0,
        x = (z / Math.tan(alpha)) || 0,
        Dell = x - d,
        // new position 
        pp = i.normal()
            .muls(Dell)
            .add(bodyA.origin[0]),
        c = bodyB.origin[0]
            .sub(pp)
            .normal()
            .muls(bodyA.radius / (bodyA.radius + bodyB.radius))
            .add(bodyA.origin[0]);
        return c;
    },
    emitter: function (bodyA, bodyB, collPointEst) {
        var 
        _calcMassRatio = function (m1, m2) {
            return (m1 && m2) ? m1 / (m1 + m2) : 0;
        };
        // this function gets iteratively called, therefore
        // needs a test for collision
        return function s(opts) {
            var 
            // point of collision
            c = collPointEst(bodyA, bodyB, opts.numIter),
            // incident vector 
            i = bodyA.projected[0]
                .sub(bodyA.origin[0])
                .sub(bodyB.projected[0].sub(bodyB.origin[0]))
                .muls(1),
            // collision normals (n)
            na = c.sub(bodyA.origin[0])
                .normal(),
            // BUG: IF BODYA LIES ON BODYB HALF, THEN UNDEFINED
            nb = c.sub(bodyB.origin[0])
                .normal(),
            // scale deflections from to 2-body conservation of momentum
            massRatio = _calcMassRatio(bodyB.mass, bodyA.mass),
            // deflect body A
            // BUG: SHOULDN'T THIS BE THE FULL DISTANCE?
            na1 = -1.0 * (i.dot(na)),
            xA = na.muls(na1 * (massRatio || 1)),
            // deflect body B
            // BUG: SHOULDN'T THIS BE THE FULL DISTANCE?
            nb1 = 1.0 * (i.dot(nb)), 
            xB = nb.muls(nb1 *(1 - massRatio));
            
            // remove penetration
            var
            dell1 = bodyA.projected[0].sub(bodyB.projected[0]).magnitude(),
            // the larger diff1 is, the more the bodies are pushed apart
            diff1 = dell1 - (bodyA.radius + bodyB.radius),
            chgxA = xA.normal()
                .muls(
                    Math.abs((diff1/2)) * opts.collisionFactor),
            chgxB = chgxA.muls(-1);
            
            // Update projected
            if ('dynamic' === bodyA.type) {
                bodyA.projected[0].x += (xA.x + chgxA.x) ;
                bodyA.projected[0].y += (xA.y + chgxA.y);
            }
            
            if ('dynamic' === bodyB.type) {
                bodyB.projected[0].x += (xB.x + chgxB.x);
                bodyB.projected[0].y += (xB.y + chgxB.y);
            }
        };
    } 
},
{
    type: ['cohesion-elem','chain-link'],
    test : function (bodyA, bodyB) {
        var 
        rA = bodyA.radius,
        rB = bodyB.radius,
        dist = rA + rB;
        return bodyA.projected[0]
            .sub(bodyB.projected[0])
            .magnitudeSqrd() <= (dist * dist);
    },
    collisionPointEst : function (bodyA, bodyB, numIter) {
        /**
        Estimates the collision point between two circle bodies.
        
        Collision point is calculated using [burk?] iterative
        estimation.
        
        Finding collision point (circle on circle ONLY):
           1) find the incident vector (p - x) 
           2) reduce incident by a FACTOR:
              
                  |incident| *= FACTOR
              
           3) loop back to step 2 until max number of iterations 
              is met, or, distance between circles is greater than
              the sum of their respective radii.
           4) return collision point
   
        @param (bodyA) Incident body. 
        @param (bodyB) "Static" body.  (Does not move.)
        @return Returns a 2D vector indicating the collision point.
        */
        var
        y = bodyB.origin[0]
            .sub(bodyA.origin[0]),
        i = bodyA.projected[0]
            .sub(bodyA.origin[0]),
        alpha = Math.acos(i.dot(y) / (y.magnitude() * i.magnitude())), 
        z = y.magnitude() * Math.sin(alpha),
        beta = Math.asin(z / (bodyA.radius + bodyB.radius)),
        d = (z / Math.tan(beta)) || 0,
        x = (z / Math.tan(alpha)) || 0,
        Dell = x - d,
        // new position 
        pp = i.normal()
            .muls(Dell)
            .add(bodyA.origin[0]),
        c = bodyB.origin[0]
            .sub(pp)
            .normal()
            .muls(bodyA.radius / (bodyA.radius + bodyB.radius))
            .add(bodyA.origin[0]);
        return c;
    },
    emitter: function (bodyA, bodyB, collPointEst) {
        var 
        _calcMassRatio = function (m1, m2) {
            return (m1 && m2) ? m1 / (m1 + m2) : 0;
        };
        // this function gets iteratively called, therefore
        // needs a test for collision
        return function s(opts) {
            var 
            // point of collision
            c = collPointEst(bodyA, bodyB, opts.numIter),
            // incident vector 
            i = bodyA.projected[0]
                .sub(bodyA.origin[0])
                .sub(bodyB.projected[0].sub(bodyB.origin[0]))
                .muls(1),
            // collision normals (n)
            na = c.sub(bodyA.origin[0])
                .normal(),
            // BUG: IF BODYA LIES ON BODYB HALF, THEN UNDEFINED
            nb = c.sub(bodyB.origin[0])
                .normal(),
            // scale deflections from to 2-body conservation of momentum
            massRatio = _calcMassRatio(bodyB.mass, bodyA.mass),
            // deflect body A
            // BUG: SHOULDN'T THIS BE THE FULL DISTANCE?
            na1 = -1.0 * (i.dot(na)),
            xA = na.muls(na1 * (massRatio || 1)),
            // deflect body B
            // BUG: SHOULDN'T THIS BE THE FULL DISTANCE?
            nb1 = 1.0 * (i.dot(nb)), 
            xB = nb.muls(nb1 *(1 - massRatio));
            
            // remove penetration
            var
            dell1 = bodyA.projected[0].sub(bodyB.projected[0]).magnitude(),
            // the larger diff1 is, the more the bodies are pushed apart
            diff1 = dell1 - (bodyA.radius + bodyB.radius),
            chgxA = xA.normal()
                .muls(
                    Math.abs((diff1/2)) * opts.collisionFactor),
            chgxB = chgxA.muls(-1);
            
            // Update projected
            if ('dynamic' === bodyA.type) {
                bodyA.projected[0].x += (xA.x + chgxA.x) ;
                bodyA.projected[0].y += (xA.y + chgxA.y);
            }
            
            if ('dynamic' === bodyB.type) {
                bodyB.projected[0].x += (xB.x + chgxB.x);
                bodyB.projected[0].y += (xB.y + chgxB.y);
            }
        };
    } 
},
{
    type: ['cohesion-elem','cohesion-elem'],
    test : function (bodyA, bodyB) {
        var 
        rA = bodyA.radius,
        rB = bodyB.radius,
        dist = rA + rB;
        return bodyA.projected[0]
            .sub(bodyB.projected[0])
            .magnitudeSqrd() <= (dist * dist);
    },
    collisionPointEst : function (bodyA, bodyB, numIter) {
        /**
        Estimates the collision point between two circle bodies.
        
        Collision point is calculated using [burk?] iterative
        estimation.
        
        Finding collision point (circle on circle ONLY):
           1) find the incident vector (p - x) 
           2) reduce incident by a FACTOR:
              
                  |incident| *= FACTOR
              
           3) loop back to step 2 until max number of iterations 
              is met, or, distance between circles is greater than
              the sum of their respective radii.
           4) return collision point
   
        @param (bodyA) Incident body. 
        @param (bodyB) "Static" body.  (Does not move.)
        @return Returns a 2D vector indicating the collision point.
        */
        var
        y = bodyB.origin[0]
            .sub(bodyA.origin[0]),
        i = bodyA.projected[0]
            .sub(bodyA.origin[0]),
        alpha = Math.acos(i.dot(y) / (y.magnitude() * i.magnitude())), 
        z = y.magnitude() * Math.sin(alpha),
        beta = Math.asin(z / (bodyA.radius + bodyB.radius)),
        d = (z / Math.tan(beta)) || 0,
        x = (z / Math.tan(alpha)) || 0,
        Dell = x - d,
        // new position 
        pp = i.normal()
            .muls(Dell)
            .add(bodyA.origin[0]),
        c = bodyB.origin[0]
            .sub(pp)
            .normal()
            .muls(bodyA.radius) // * (bodyA.radius / (bodyA.radius + bodyB.radius)))
            .add(bodyA.origin[0]);
        return c;
    },
    emitter: function (bodyA, bodyB, collPointEst) {
        var 
        _calcMassRatio = function (m1, m2) {
            return (m1 && m2) ? m1 / (m1 + m2) : 0;
        };
        // this function gets iteratively called, therefore
        // needs a test for collision
        return function s(opts) {
            var 
            // point of collision
            c = collPointEst(bodyA, bodyB, opts.numIter),
            // incident vector 
            i = bodyA.projected[0]
                .sub(bodyA.origin[0])
                .sub(bodyB.projected[0].sub(bodyB.origin[0]))
                .muls(1),
            // collision normals (n)
            na = c.sub(bodyA.origin[0])
                .normal(),
            // BUG: IF BODYA LIES ON BODYB HALF, THEN UNDEFINED
            nb = c.sub(bodyB.origin[0])
                .normal(),
            // scale deflections from to 2-body conservation of momentum
            massRatio = _calcMassRatio(bodyB.mass, bodyA.mass),
            // deflect body A
            // BUG: SHOULDN'T THIS BE THE FULL DISTANCE?
            na1 = -.65 * (i.dot(na)),
            xA = na.muls(na1 * (massRatio || 1)),
            // deflect body B
            // BUG: SHOULDN'T THIS BE THE FULL DISTANCE?
            nb1 = .65 * (i.dot(nb)), 
            xB = nb.muls(nb1 *(1 - massRatio));
            
            // remove penetration
            var
            dell1 = bodyA.projected[0].sub(bodyB.projected[0]).magnitude(),
            // the larger diff1 is, the more the bodies are pushed apart
            diff1 = dell1 - (bodyA.radius + bodyB.radius),
            chgxA = xA.normal()
                .muls(
                    Math.abs((diff1/2)) * opts.collisionFactor),
            chgxB = chgxA.muls(-1);
            
            // Update projected
            if ('dynamic' === bodyA.type) {
                bodyA.projected[0].x += (xA.x + chgxA.x) ;
                bodyA.projected[0].y += (xA.y + chgxA.y);
            }
            
            if ('dynamic' === bodyB.type) {
                bodyB.projected[0].x += (xB.x + chgxB.x);
                bodyB.projected[0].y += (xB.y + chgxB.y);
            }
        };
    } 
}];

function _newWorld() {
     
    var
    bodies = Bodies(),
    collisionConstraintMgr = CollisionConstraintEmitter(
        // TODO: Can registering replace this declaration?
        ['circle','line','chain','chain-link','cohesion-elem']),
    clusterConstraintMgr = ClusterConstraintEmitter(
        // TODO: Can registering replace this declaration?
        ['chain-link','cohesion','circle']);

    collisionConstraintMgr.register(collisionConstraints);
    clusterConstraintMgr.register(clusterConstraints);

    return World(
        bodies, 
        PhysicsEngine(
            bodies, 
            collisionConstraintMgr,
            clusterConstraintMgr));
}

var
_worlds = {};

window.ML = Object.freeze({
    createNewWorld: function (id) {
        var world = _newWorld();
        _worlds[id] = world;
        return world;
    },
    getWorld: function (id) {
        return _worlds[id];
    }
});

})();
