(function () {
"use strict";

var
_util = Object.freeze({    
    toggle: function (on_func, off_func, state) {
        state = state || false;
        return function (obj) {
            state = !state;
            return (state) ? on_func(obj) : off_func(obj);
        };
    }
});

function createDanglingChain(obj, world, panel) {
    
    var
    mass = obj.mass || 10,
    origx = obj.origx || 10,
    origy = obj.origy || 25,
    velx = obj.velx || 0,
    vely = obj.vely || 0,
    offx = obj.offx || 25,
    numLinks = obj.numLinks,
    stiffness = obj.stiffness || .95,
    dangling = obj.dangling;
    
    var c = world.createNewBody({
        shape: 'chain',
        draw: function (b) {
            b.elems.forEach(function (l) {
                DRAW(panel)
                    .circle(l.origin[0].x, l.origin[0].y, l.radius)
                    .fillStyle('#555555')
                    .text(l.id, l.origin[0].x-3, l.origin[0].y+3);
            });
       }
    });
    
    for (var i = 0; i < numLinks; i++) {
        var l = world.createNewBody({
            id: i,
            shape:'chain-link',
            stiffness: stiffness,
            // heavy mass for less bounciness (less jitter/heaving), 
            // longer links for elongation
            mass: mass,
            linkLength: obj.linkLength || ((obj.radius || 10) * 2),
            // small radii causes shivering
            radius: obj.radius || 10,
            type: (0 === i) ? ((dangling) ? 'static':'dynamic'):'dynamic',
            origin: [origx + i*offx, origy],
            velocity: [velx,vely]
        });
        c.elems.push(l);
    }
    return c;
}

var
sprites = function (obj) {
    var
    _img = {};
    
    function loader(obj) {
        Object.keys(obj).forEach(function (k) {
            var
            img = new Image();
            img.src = obj[k];
            _img[k] = img;
            img = null;
        });
    }
    
    loader(obj);
    
    return Object.freeze({
        get: function (name) {
            return _img[name];
        }
    });
};

var
sp = sprites({
    moon: 'img/moon_30.png',
    cloud1: 'img/cloud_1_25.png',
    cloud2: 'img/cloud_2_25_20.png',
    cloud3: 'img/cloud_3_25.png'
});

function createChain(obj, world, id) {
    
    var
    origx = obj.origx || 10,
    origy = obj.origy || 25,
    velx = obj.velx || 0,
    vely = obj.vely || 0,
    offx = obj.offx || 25,
    numLinks = obj.numLinks,
    stiffness = obj.stiffness || .95;
    var c = world.createNewBody({
        shape: 'chain',
        draw: function (b) {
            b.elems.forEach(function (l) {
                var 
                x = l.id,
                cloud = ((x % 3) == 0) ? sp.get('cloud3') : ((x % 2) == 0) ? sp.get('cloud2') : sp.get('cloud1');
                DRAW(id)
                    //.circle(l.origin[0].x, l.origin[0].y, l.radius)
                    //.fillStyle('#555555')
                    //.text(l.id, l.origin[0].x-3, l.origin[0].y+3)
                    .image(cloud, l.origin[0].x-15, l.origin[0].y-13, 30, 25);
                cloud = null;
            });
       }
    });

    for (var i = 0; i < numLinks; i++) {
        var l = world.createNewBody({
            id: i,
            shape:'chain-link',
            stiffness: stiffness,
            // heavy mass for less bounciness (less jitter/heaving), 
            // longer links for elongation
            mass: 200,
            linkLength: 20,
            // small radii causes shivering
            radius: 10,
            type: (0 == i || numLinks - 1 == i) ? 'static':'dynamic',
            origin: [origx + i*offx, origy],
            velocity: [velx,vely]            
        });
        c.elems.push(l);
    }
    return c;
}

function createCohesion(obj, world, panelname) {
    
    var
    origx = obj.origx || 10,
    origy = obj.origy || 25,
    velx = obj.velx || 0,
    vely = obj.vely || 0,
    offx = obj.offx || 25,
    aorigy = obj.aorigy || 150,
    numLinks = obj.numLinks;
    
    var c = world.createNewBody({
        id: obj.id,
        shape: 'cohesion',
        type: 'ghost',
        minDistance: obj.minDistance,
        origin: [obj.aorigx, obj.aorigy],
        velocity: [velx, vely],
        draw: function (b) {
            b.elems.forEach(function (l) {
                DRAW(panelname)
                    .circle(l.origin[0].x, l.origin[0].y, l.radius)
                    .fillStyle(obj.color)
                    .fill();
            });
       }
    });
    
    for (var i = 0; i < numLinks; i++) {
        var l = world.createNewBody({
            id: i,
            shape:'cohesion-elem',
            stiffness: .75,
            mass: 70,
            damping: obj.damping,
            radius: obj.radius + Math.floor(Math.random() *10),
            type: 'dynamic',
            origin: [origx + i*offx, origy],
            velocity: [velx,0]
        });
        c.elems.push(l);
    }
    return c;
}

function createCircles(obj, world, id) {
    
    var
    num = obj.num,
    lvl = obj.lvl,
    radius = obj.radius,
    velocity = obj.velocity || [0,0];
    
    for (var i = 0; i < num; i++) {
        for (var j = 0; j < lvl; j++) {
        
            var 
            radius = Math.max(radius, 4+Math.random()*10),
            circ = world.createNewBody({
                radius: radius,
                id: j,
                // TODO: the mass is doing something funky...
                mass: 100,
                shape:'circle',
                type: obj.type || 'dynamic',
                origin: [obj.x || (100 + i*30), obj.y || (90 - j*30)],
                velocity: velocity,
                draw: function (l) {
                    DRAW(id)
                        .circle(l.origin[0].x, l.origin[0].y, l.radius)
                        .fillStyle(('static' == l.type) ? '#555555' : '#999900')
                        .text(l.id, l.origin[0].x-3, l.origin[0].y+3);
            }});
            
            world.add(circ);
        }
    }
}


function createMoon(obj, world, id) {
    
    var
    num = obj.num,
    lvl = obj.lvl,
    radius = obj.radius,
    velocity = obj.velocity || [0,0];
    
    for (var i = 0; i < num; i++) {
        for (var j = 0; j < lvl; j++) {
        
            var 
            radius = Math.max(radius, 4+Math.random()*10),
            circ = world.createNewBody({
                radius: radius,
                id: j,
                // TODO: the mass is doing something funky...
                mass: 100,
                shape:'circle',
                type: obj.type || 'dynamic',
                origin: [obj.x || (100 + i*30), obj.y || (90 - j*30)],
                velocity: velocity,
                draw: function (l) {
                    DRAW(id)
                        //.circle(l.origin[0].x, l.origin[0].y, 13)
                        //.fillStyle(('static' == l.type) ? '#555555' : '#999900')
                        //.text(l.id, l.origin[0].x-3, l.origin[0].y+3)
                        .image(sp.get('moon'), l.origin[0].x-18, l.origin[0].y-15, 35, 30);
            }});
            
            world.add(circ);
        }
    }
}

function setupWorld(setupFunc) {

    Object.keys(setupFunc).forEach(function (id) {
        
        var world = ML.createNewWorld(id);
        setupFunc[id](id, world);
    
        world.execute(function () {
            world.step(.05);
            DRAW(id).clear();
            world.draw();
        });
        
        // connect world to controls
        document.getElementById(id +'_play')
            .addEventListener(
                'click', _util.toggle(
                    function (event) {
                        event.target.value = 'Stop';
                        world.start();
                    },
                    function (event) {
                        event.target.value = 'Start';
                        world.stop();
                    })); 

        document.getElementById(id +'_reset')
            .addEventListener(
                'click', 
                function (event) {
                    world.clear();
                    setupFunc[id](id, world);
                    world.draw();
                }); 
        // see the world mom!
        world.draw();
    }); 
}

function eventManager() {
    
    var 
    _events = {};
    
    return Object.freeze({
        'add': function (obj) {
            _events[obj.eventId] = obj.handler;
            document.getElementById(obj.id)
                .addEventListener(obj.type, _events[obj.eventId]);
        },
        'remove': function (eventId, type, id) {
            if (!_events[eventId]) 
                return;
            document.getElementById(id)
                .removeEventListener(type, _events[eventId]);
            _events[eventId] = null;
        }
    });
};

var
simpleEvntMgr = eventManager();

// each world is identified by the key
setupWorld({
    'chain_panel': function (id, world) {
        
        DRAW(id).dim(450,275);
        
        // BUG: the stiffness factor reduces velocity
        
        world.add(createDanglingChain({
            dangling: true,
            origx: 150, origy: 10,
            linkLength: 18,
            stiffness: .75,
            radius:10,
            numLinks: 9 }, world, id));
            
        world.add(createDanglingChain({
            dangling: true,
            origx: 275, origy: 10,
            offx: -25,
            stiffness: .98,
            numLinks: 8 }, world, id));
        
        world.add(createDanglingChain({
            mass: 5,
            dangling: false,
            origx: 330, origy: 10,
            linkLength: 21,
            stiffness: .80,
            radius:10,
            numLinks: 8 }, world, id));
        
        world.add(createCohesion({
            origx: 400, origy: 160,
            velx: -5, vely: 0,
            offx: 30,
            minDistance: 25,
            numLinks: 5,
            aorigx: 400, aorigy: 110,
            radius: 5,
            damping: 0.025,
            color: '#DD0000' }, world, id));
            
        world.setGravity(0,5000);
    },
    'bridge_panel': function (id, world) {
        
        simpleEvntMgr.remove('bridge-add-circle', 'click', id);
        
        simpleEvntMgr.add(
        {
            eventId: 'bridge-add-circle',
            type: 'click',
            id: id,
            handler: function s(event) {
                createMoon({ 
                    num: 1, lvl: 1, 
                    radius: 5,
                    x: event.offsetX,
                    y: event.offsetY }, world, id);
                DRAW(id).clear();
                world.draw();
            }
        });
        
        DRAW(id).dim(450,350);

        world.add(
            createChain({
                origx: 10,
                origy: 135,
                stiffness: .9,
                numLinks: 18 }, world, id));
/* DEBUG        
        createCircles({ 
            num: 5, lvl: 3, radius: 5 }, world, id);
*/        
        world.setGravity(0,5000);
    
    },
    'cohesion_panel': function (id, world) {
        
        DRAW(id).dim(450,325);
        
        world.add(createCohesion({
            id: 'A',
            origx: 10, origy: 150,
            velx: 10, vely: 2,
            offx: 25,
            minDistance: 40,
            numLinks: 15,
            aorigx: 100, aorigy: 145,
            radius: 7,
            damping: 0.0075,
            color: '#DD0000' }, world, id));

        world.add(createCohesion({
            id: 'B',
            origx: 120, 
            velx: -10,
            offx: 25,
            minDistance: 55,
            numLinks: 20,
            aorigx: 350, aorigy: 145,
            radius: 7,
            damping: 0.09,
            color: '#0088aa' }, world, id));

        world.add(createCohesion({
            id: 'C',
            origx: 120, origy: 300,
            vely: -10,
            offx: 25,
            minDistance: 25,
            numLinks: 10,
            aorigx: 225, aorigy: 250,
            radius: 7,
            damping: 0.01675,
            color: '#aa8800' }, world, id));
    }
});
    
})();