/**
Basic drawing primitives. For debugging.
*/
(function(undefined) {
	
	function draw(canvasId) {
		this._canvasId = canvasId;
		this._context = "2d";
	};
	
	draw.prototype = {
		
		circle : function(x,y,r) {
            /**
            Draws an unfilled circle.
            
            @param x A x-axis canvas value.
            @param y A y-axis canvas value.
            @param r Circle radius in pixels.
            */
			var 
			canvas = document.getElementById(this._canvasId),
			ctx =  canvas.getContext(this._context);
			ctx.beginPath();
			ctx.arc(x, y, r, 0, Math.PI*2, true);
			ctx.strokeStyle = 'black';
			ctx.stroke();
			canvas = ctx = null;
			return this;
		},
		clear : function() {
            /**
            Clears the canvas.
            */
			var 
			canvas = document.getElementById(this._canvasId),
			ctx =  canvas.getContext(this._context);
			
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			canvas = ctx = null;
			return this;
		},
		mark : function (x,y) {
            /**
            Creates a cross-hair mark at the given x,y on the canvas.
            
            @param x A x-axis canvas value.
            @param y A y-axis canvas value.
            @param color Color of the cross-hair. Default is 'red'.
            */
			var 
			canvas = document.getElementById(this._canvasId),
			ctx =  canvas.getContext(this._context),
			len = 3;
			
			ctx.beginPath();
			ctx.moveTo(x - len,y);
			ctx.lineTo(x + len,y);
			//ctx.stroke('red');
			
			ctx.beginPath();
			ctx.moveTo(x,y-len);
			ctx.lineTo(x,y+len);
			//ctx.stroke('red'); 
			
			canvas = ctx = null;
			return this;
		},
		dim : function(width, height) {
            /**
            Dimensions the canvas.
            
            @param New canvas width in pixels.
            @param New canvas height in pixels.
            */
			var 
			canvas = document.getElementById(this._canvasId);
			canvas.width = width;
			canvas.height = height;
			canvas = null;
			return this;
		},
		debug : function () {
			console.log(document.getElementById(this._canvasId));
		},
		fillStyle : function (color) {
            /**
            Sets the fill color.
            
            @param color Current fill color
            */
			var 
			canvas = document.getElementById(this._canvasId),
			ctx =  canvas.getContext(this._context);
			ctx.fillStyle = (color) ? color : ctx.fillStyle;
			ctx = canvas = null;
			return this;
		},
		fill : function (color) {
			var 
			canvas = document.getElementById(this._canvasId),
			ctx =  canvas.getContext(this._context);
			ctx.fill();
			ctx = canvas = null;
			return this;
		},
		line : function (pt1, pt2, color) {
			/**
            Creates an line with highlighted endpoints.
            
            @param pt1 A vector object containing x,y pixel coordinates.
            This point is highlighted as a red dot.
            @param pt2 A vector object containing x,y pixel coordinates.
            This point is highlighted as a blue dot.            
            */
			
            // highlights line endpoints
			this
				.circle(pt1.x, pt1.y, 2)
				.fillStyle('red')
				.fill()
				.circle(pt2.x, pt2.y, 2)
				.fillStyle('blue')
				.fill();
			var
			canvas = document.getElementById(this._canvasId),
			ctx =  canvas.getContext(this._context);
			
			ctx.beginPath();
			ctx.moveTo(pt1.x,pt1.y);
			ctx.lineTo(pt2.x,pt2.y);
			ctx.strokeStyle = color;
			ctx.stroke(); 
			
			ctx = canvas = null;
			
			return this;
		},
		text : function (text, x, y) {
			var
			canvas = document.getElementById(this._canvasId),
			ctx =  canvas.getContext(this._context);
			
			ctx.fillText(text, x, y);
			
			ctx = canvas = null;
			return this;
		},
        image : function (img, x, y, w, h) {
          	var
			canvas = document.getElementById(this._canvasId),
			ctx =  canvas.getContext(this._context);
            ctx.drawImage(img, x, y, w, h);
            canvas = ctx = null;
            return this;
        }
	};
	
	function ctor(id) {
		return new draw(id);
	}
	
	window.DRAW = ctor;
})();
